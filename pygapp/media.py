import os
import pygame

RACINE_REP = None
MEDIA_REP  = None
SAUVE_REP = None
IMAGE_REP = None
SON_REP = None
TEST_REP = None

DOSSIER_SAUVEGARDES = 'saves' #'sauvegardes'
DOSSIER_IMAGES      = 'images'
DOSSIER_SONS        = 'sounds' #'sons'

def setupResourcePath(mediaFile):
    global RACINE_REP
    RACINE_REP  = os.path.split(os.path.realpath(mediaFile))[0]
    
    if '.exe' in RACINE_REP:
        RACINE_REP = os.path.split(RACINE_REP)[0]
         
    #print 'RACINE_REP', RACINE_REP
    
    global MEDIA_REP
    MEDIA_REP   = os.path.join(RACINE_REP, 'media')
    
    global SAUVE_REP
    global IMAGE_REP
    global SON_REP
    SAUVE_REP   = os.path.normpath(os.path.join(RACINE_REP, DOSSIER_SAUVEGARDES))
    IMAGE_REP   = os.path.normpath(os.path.join(MEDIA_REP, DOSSIER_IMAGES))
    SON_REP     = os.path.normpath(os.path.join(MEDIA_REP, DOSSIER_SONS))
    

FONTE_DEFAUT = "freesansbold.ttf"

_CacheSon   = {}
_CacheImage = {}

def VidangeCache():
    _CacheImage.clear()
    _CacheSon.clear()


class MediaManquantExc(Exception):
    
    def __init__(self, nomFichier):
        self.nomFichier = nomFichier
        
    def __repr__(self):
        return '%s introuvable'%self.nomFichier
    
    __str__ = __repr__

def cheminFichier(nomFichier, subdir='', verifExiste=True):
     
    nomFichierLong = os.path.join(MEDIA_REP, subdir, nomFichier)
    if verifExiste and not os.path.exists(nomFichierLong):
        raise MediaManquantExc(nomFichierLong)

    return nomFichierLong


def charge_image_no_cache(nomFichier,scale=1):
        
    nomFichierComp = cheminFichier(nomFichier, subdir='images')
    
    try:
        
        image = pygame.image.load(nomFichierComp)
        image = pygame.transform.scale(image, (int(image.get_width()*scale), int(image.get_height()*scale)))
        
    except pygame.error:
        import traceback
        traceback.print_exc()
            
    return image.convert_alpha()
    
def charge_image(nomFichier,scale=1):
    
    if (nomFichier,scale) not in _CacheImage:        
        _CacheImage[(nomFichier,scale)] = charge_image_no_cache(nomFichier,scale)

    return _CacheImage[(nomFichier,scale)]


class EffetSonore( pygame.mixer.Sound ):

    def __init__( self, nomFichier, volume ):
        
        self.__safe_for_unpickling__ = True
        
        nomFichier = cheminFichier(nomFichier, subdir='sons')
        pygame.mixer.Sound.__init__(self, nomFichier)
        
        self.nomFichier = nomFichier
        self.volume = volume
        
        self.set_volume(volume)

        global _CacheSon
        if nomFichier not in _CacheSon:
            _CacheSon[nomFichier] = self
    
    def __str__(self):
        return pygame.mixer.Sound.__str__(self), self.nomFichier
    
    def __reduce__(self):
        return EffetSonore, (self.nomFichier, self.volume)
    
    '''
    def __getnewargs__(self):
        return self.nomFichier, self.volume
    '''
    def __getstate__(self):
        return dict( nomFichier = self.nomFichier, volume = self.volume, __safe_for_unpickling__=True )
    '''
    def __setstate__(self,Dict):
        self.__init__( **Dict )
        
    ''' 
   
def charge_son_no_cache(nomFichier, volume=0.5):

    nomFichierComp = cheminFichier(nomFichier, subdir='sons')
    
    sound = pygame.mixer.Sound(nomFichierComp)
    sound.set_volume(volume)
           
    return sound
    
def charge_son(nomFichier, volume=0.5):

    global _CacheSon
    if nomFichier not in _CacheSon:
        _CacheSon[nomFichier] = EffetSonore(nomFichier, volume)
        
    return _CacheSon[nomFichier]