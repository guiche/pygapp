'''
Created on 11 Apr 2013

@author: j913965
'''
from pygapp import sauvegarde
from pygapp.sauvegarde import Phenixable
import copy

def DiffParties(partie1, partie2, loquace=True):
    """
        :type partie1: canopee.racine.Partie
        :type partie2: canopee.racine.Partie
    """
    different = sauvegarde.diff(partie1, partie2, loquace=loquace)
        
    if different and loquace:
        print
        print 'Etats differents'
        print different

    return different


class PartieComparee(Phenixable):
    
    SUFFIX    = '.par_comp'
    JALON_INIT= '_PartieCompareJalonInit'
    
    def __init__(self, partieInit=None, partieFin=None):
        """
            :type partieInit: canopee.racine.Partie
            :type partieFin: canopee.racine.Partie
        """
        self.nom       = '' if partieInit is None else partieInit.nom
        self.EtatInit  = partieInit
        self.EtatFinal = copy.deepcopy(partieFin)
        
    def Deroule(self):
        numToursAderouler = self.EtatFinal.NumeroDuTour - self.EtatInit.NumeroDuTour
        print
        print '%s Deroule %d tours de %d a %d'%(self.nom, numToursAderouler, self.EtatInit.NumeroDuTour, self.EtatFinal.NumeroDuTour)
        EtatDeroule = copy.deepcopy(self.EtatInit)
        
        EtatDeroule.Enregistrement = self.EtatFinal.Enregistrement
        # efface les evenements produits a ce tour
        EtatDeroule.Enregistrement.pop(self.EtatInit.NumeroDuTour,None)
        EtatDeroule.Rediffusion = numToursAderouler
        EtatDeroule.GrandBoucle(MaxNombTours=numToursAderouler)
        EtatDeroule.Rediffusion = 0
        return EtatDeroule
    
    def record(self):
        pass

    def Rebase(self):
        self.EtatFinal = self.Deroule()
    
    def DiffParties(self):
        EtatDeroule = self.Deroule()
        different = DiffParties(self.EtatFinal, EtatDeroule)
        
        if different:
            EtatDeroule.InitPreBoucle()
            Surf = EtatDeroule.Surf
            self.EtatFinal.Surf = Surf
            import pygame
            pygame.display.set_caption('%s apres deroulage %d tours'%(EtatDeroule.nom, self.EtatFinal.NumeroDuTour - self.EtatInit.NumeroDuTour))
            Courir = True
            
            while Courir:
                            
                Surf.fill((0, 0, 0))
                Surf.set_alpha()
                EtatDeroule.PeintUneToile()
                Surf2 = Surf.copy()
                Surf.fill((0, 0, 0))
                self.EtatFinal.PeintUneToile()
                #Surf2.fill((200,0,0))
                Surf2.convert_alpha()
                Surf2.set_alpha(128)
                Surf.blit(Surf2,(0,0))
                pygame.display.flip()

                for eve in pygame.event.get():
                    if eve.type == pygame.KEYUP and eve.key == pygame.K_ESCAPE:
                        Courir = False
                        break
                    
        return different
    