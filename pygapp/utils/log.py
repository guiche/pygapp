'''
Created on May 15, 2015

@author: gg99
'''
import time

class TraceLoggable(object):
    
    def __init__(self, log=True):
        
        self.resetLogTime()
        self._doLog = log
        self._logDepth = 0

    def resetLogTime(self):
        self._logStartTime = time.time()
        
    def printLog(self,*args):
        if self._doLog:            
            elapsedTime = time.time() - self._logStartTime
            indent = '\t' * self.indentLevel
            timeStr = "[%.2f]%s"%(elapsedTime, indent)
            print timeStr, ' '.join(str(arg) for arg in args)

def traceLog(comment=''):
        
    def logger_decorator(func):
        
        def f(loggable, *args, **kwargs):
            
            aargs = list(args) + [str(name) + '=' + str(val) for name, val in kwargs.iteritems()]
            argsStr = '(' + ', '.join(str(arg) for arg in aargs) + ')'
            if comment:
                argsStr += '# ' + comment 
            
            loggable.printLog('--'+func.__name__+argsStr)
            loggable.indentLevel += 1
            
            res = func(loggable, *args, **kwargs)            
            loggable.indentLevel -= 1
            
            return res
        
        return f
    
    return logger_decorator

   
class suppressLog(object):
    
    def __init__(self, loggable, **kwargs):
        self.loggable = loggable
        
    def __enter__(self):
        self.loggable.printLog("stop log")
        self.loggable._doLog = False
        
    def __exit__(self, _type, value, traceback):
        self.loggable._doLog = True
        self.loggable.printLog("start log")
