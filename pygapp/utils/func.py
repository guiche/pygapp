'''
Created on 21 May 2014

@author: j913965
'''


class partial(object):
    def __init__(self,f,*args,**kwargs):
        self.f = f
        self.args = args
        self.kwargs = kwargs

    def __call__(self,*restargs,**restkwargs):
        kwargs = self.kwargs.copy()
        kwargs.update( restkwargs )

        return self.f( *(self.args+restargs), **kwargs )

    def __getstate__(self):
        f = self.f
        if type(f) != types.FunctionType:
            raise ValueError( 'Can only pickle functions not %s' % (f,) )
        return ((f.__module__,f.func_name), self.args, self.kwargs )

    def __setstate__(self,state):
        modname, funcname = state[0]
        __import__( modname )

        self.f = getattr( sys.modules[ modname ], funcname )
        self.args, self.kwargs = state[1:]

    def __repr__(self):
        args   = map(repr, self.args) + ['%s=%r' % kv for kv in self.kwargs.iteritems()]
        argStr = '( ' + ', '.join(args) + ' )'
        return '%s%s' % (lib.format.callable(self.f), argStr)

