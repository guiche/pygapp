'''
Created on 21 May 2014

@author: j913965
'''

#
# Copyright 2008 Jose Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

'''Visualize dot graphs via the xdot format.'''

import colorsys
import math
import os
import re
import subprocess
import sys
import time
import traceback
import wx
import tempfile


# See http://www.graphviz.org/pub/scm/graphviz-cairo/plugin/cairo/gvrender_cairo.c

# For pygtk inspiration and guidance see:
# - http://mirageiv.berlios.de/
# - http://comix.sourceforge.net/

def asg_graphics_color( color ):
    if isinstance(color, tuple):
        r = color[0]
        g = color[1]
        b = color[2]
        a = color[3]
    elif isinstance(color, wx.Colour):
        r = color.Red()
        g = color.Green()
        b = color.Blue()
        a = color.Alpha()
    return ( a << 24 ) | ( r << 16 ) | ( g << 8 ) | ( b << 0 )

class GraphicsColor:
    def __init__(self, color, linewidth=1):
        self.Colour = color
        self.linewidth = linewidth

class Pen:
    """Store pen attributes."""
    def __init__(self):
        # set default attributes
        self.color = (0, 255, 0, 255)
        self.fillcolor = (0, 0, 255, 255)
        self.linewidth = 1.0
        self.fontsize = 10.0
        #self.fontname = "Arial"
        self.fontname = "Tahoma"
        self.dash = ()
        self.font = None
        self.pen = None
        self.brush = None

    def copy(self):
        """Create a copy of this pen."""
        pen = Pen()
        pen.__dict__ = self.__dict__.copy()
        return pen

    def highlighted(self):
        pen = self.copy()
        pen.color = (255, 0, 0, wx.ALPHA_OPAQUE)
        pen.fillcolor = (255, 0, 0, wx.ALPHA_OPAQUE)
        return pen

    def wxpen(self):
        #self.color = [ int( 255*x ) for x in self.color ]
        if not self.pen:        
            ## XXX: Might want to remove this, but small linewidths don't look right now
            self.linewidth = max( 1, self.linewidth )
            self.pen = wx.Pen(wx.Colour(*self.color), self.linewidth)
            
        return self.pen

    def wxbrush(self):
        if not self.brush:
            self.brush = wx.Brush(wx.Colour(*self.fillcolor))
            
        return self.brush

    def wxfont( self ):
        if not self.font:
            self.font = wx.FFont( self.fontsize, family=wx.SWISS, face=self.fontname )
        return self.font


class Shape:
    """Abstract base class for all the drawing shapes."""
    def __init__( self ):
        pass

    def getText( self ):
        return ''

    def draw(self, cr, highlight=False):
        """Draw this shape with the given cairo context"""
        raise NotImplementedError

    def select_pen(self, highlight):
        if highlight:
            if not hasattr(self.pen, 'highlight_pen'):
                self.pen.highlight_pen = self.pen.highlighted()
            return self.pen.highlight_pen.wxpen(), self.pen.highlight_pen.wxbrush()
        else:
            if not self.pen.pen:
                self.pen.pen = self.pen.wxpen()
                self.pen.brush = self.pen.wxbrush()
            return self.pen.pen, self.pen.brush


class TextShape(Shape):
    LEFT, CENTER, RIGHT = -1, 0, 1

    def __init__(self, pen, x, y, j, w, t):
        Shape.__init__(self)
        self.pen = pen.copy()
        self.x = x
        self.y = y
        self.j = j
        self.w = w
        self.t = t
        self.wxfont = None
        self.align = { self.LEFT: wx.ALIGN_LEFT, self.RIGHT: wx.ALIGN_RIGHT, self.CENTER: wx.ALIGN_CENTER }[j]

    def getText( self ):
        return self.t

    def draw(self, cr, highlight=False):
        #print 'TextShape:', self.t, self.pen.fontname, self.pen.fontsize, self.pen.color
        # show where dot thinks the text should appear
        #cr.set_source_rgba(1, 0, 0, .9)
        if self.j == self.LEFT:
            x = self.x
        elif self.j == self.CENTER:
            x = self.x - 0.5*self.w
        elif self.j == self.RIGHT:
            x = self.x - self.w

        #path = cr.CreatePath()
        #path.MoveToPoint(x, self.y)
        #path.AddLineToPoint(x+self.w, self.y)
        #cr.SetPen(wx.BLACK_PEN)
        #cr.StrokePath(path)

        #font = self.pen.fontsize, face=self.pen.fontname )
        if not self.wxfont:
            self.wxfont = self.pen.wxfont()
        cr.SetFont( self.wxfont )

        cr.SetTextForeground( wx.Colour( *self.pen.color ) )
        #cr.DrawText(self.t, x, self.y - self.pen.fontsize)
        pen = wx.Pen( wx.RED )
        pen.SetWidth( 1 )
        cr.SetPen( pen )
        #tw, th = cr.GetTextExtent( self.t )
        rect = wx.Rect( x, self.y-self.pen.fontsize, self.w, self.pen.fontsize )

        #cr.DrawLine( x, self.y, x+self.w, self.y )
        #cr.SetBrush( wx.TRANSPARENT_BRUSH )
        #cr.DrawRectangleRect( rect )

        cr.DrawLabel( self.t, rect, self.align )

    def render( self, graphics, highlight=False ):
        if self.j == self.LEFT:
            x = self.x
        elif self.j == self.CENTER:
            x = self.x - 0.5*self.w
        elif self.j == self.RIGHT:
            x = self.x - self.w
        graphics.set_text_color( asg_graphics_color(self.pen.color) )
        # FIX: the text coordinates should just be the rectangle polygon
        graphics.fitted_text_out( x, self.y, x + self.w +4, self.t )


class EllipseShape(Shape):

    def __init__(self, pen, x0, y0, w, h, filled=False):
        Shape.__init__(self)
        self.pen = pen.copy()
        self.x0 = x0
        self.y0 = y0
        self.w = w
        self.h = h
        self.filled = filled

    def draw(self, cr, highlight=False):
        #print 'EllipseShape'
        #return
        #saved = cr.GetTransform()
        #cr.Translate(self.x0, self.y0)
        #cr.Scale(self.w, self.h)
        #path = cr.CreatePath()
        #path.MoveToPoint(1.0, 0.0)
        #path.AddArcToPoint(0.0, 0.0, 1.0, 0, 2.0*math.pi)
        #cr.SetTransform(saved)

        pen, brush = self.select_pen(highlight)
        cr.SetPen(pen)
        cr.SetBrush(brush)

        cr.DrawEllipse(self.x0-self.w, self.y0-self.h, self.w*2, self.h*2)
        #if self.filled:
            #cr.FillPath(path)
        #else:

            #cr.set_dash(pen.dash)
            #cr.set_line_width(pen.linewidth)
            #cr.set_source_rgba(*pen.color)
            #cr.StrokePath(path)

    def render( self, graphics, highlight=False ):
        pen, brush = self.select_pen(highlight)
        graphics.set_pen( asggraphics.ASGGraphics.GraphicsRecordInt.pen_type_solid, pen.linewidth, asg_graphics_color(pen.Colour) )
        if self.filled:
            graphics.set_brush( asggraphics.ASGGraphics.GraphicsRecordInt.brush_type_solid, asg_graphics_color(brush.Colour) )
        else:
            graphics.set_brush( asggraphics.ASGGraphics.GraphicsRecordInt.brush_type_null, 0 )
        left = self.x0-self.w
        top = self.y0-self.h
        right = left + self.w*2
        bottom = top + self.h*2
        graphics.draw_ellipse( left, top, right, bottom )

class PolygonShape(Shape):

    def __init__(self, pen, points, filled=False):
        Shape.__init__(self)
        self.pen = pen.copy()
        self.points = points
        self.filled = filled

    def render( self, graphics, highlight=False ):
        pen, brush = self.select_pen(highlight)
        graphics.set_pen( asggraphics.ASGGraphics.GraphicsRecordInt.pen_type_solid, pen.linewidth, asg_graphics_color(pen.Colour) )
        if self.filled:
            graphics.set_brush( asggraphics.ASGGraphics.GraphicsRecordInt.brush_type_solid, asg_graphics_color(brush.Colour) )
        else:
            graphics.set_brush( asggraphics.ASGGraphics.GraphicsRecordInt.brush_type_null, 0 )
        graphics.draw_polygon( self.points )

    def draw(self, cr, highlight=False):
        pen, brush = self.select_pen(highlight)
        cr.SetPen( pen )
        if self.filled:
            cr.SetBrush( brush )
        else:
            cr.SetBrush( wx.TRANSPARENT_BRUSH )

        cr.DrawPolygon( self.points )

        '''
        pts = self.points
        if 4==len( pts ) and pts[0][1]==pts[1][1] and pts[1][0] == pts[2][0] and pts[2][1]==pts[3][1] and pts[3][0]==pts[0][0]:
            ## rect
            x = min( pt[0] for pt in pts )
            y = min( pt[1] for pt in pts )
            w = max( pt[0] for pt in pts ) - x
            h = max( pt[1] for pt in pts ) - y
            #print 'rect:', x, y
            cr.DrawRectangle( x, y, w, h )
        else:
            ## some other polygon
            #print 'PolygonShape', self.points
            path = cr.CreatePath()
            x0, y0 = self.points[-1]
            path.MoveToPoint(x0, y0)
            for x, y in self.points:
                path.AddLineToPoint(x, y)
                path.MoveToPoint(x, y)
            path.AddLineToPoint(x0, y0)

            path.CloseSubpath()

            if self.filled:
                cr.DrawPath( path )
            else:
                cr.StrokePath(path)
            '''


class LineShape(Shape):

    def __init__(self, pen, points):
        Shape.__init__(self)
        self.pen = pen.copy()
        self.points = points

    def render( self, graphics, highlight=False ):
        graphics.set_pen( asggraphics.ASGGraphics.GraphicsRecordInt.pen_type_solid, self.pen.linewidth, asg_graphics_color(self.pen.color) )
        graphics.draw_curve( self.points )

    def draw(self, cr, highlight=False):
        pen, _brush = self.select_pen(highlight)
        cr.SetPen( pen )
        cr.DrawLines( self.points )
        return

        '''
        x0, y0 = self.points[0]
        cr.move_to(x0, y0)
        for x1, y1 in self.points[1:]:
            cr.line_to(x1, y1)
        pen = self.select_pen( highlight )
        cr.set_dash( pen.dash )
        cr.set_line_width( pen.linewidth )
        cr.set_source_rgba( *pen.color )
        cr.stroke()
        '''


class BezierShape(Shape):

    def __init__(self, pen, points, filled=False):
        Shape.__init__(self)
        self.pen = pen.copy()
        self.points = points
        self.filled = filled

    def draw(self, cr, highlight=False):
        ## Note. This will not render a Bezier curve or anything close!
        pen, _brush = self.select_pen(highlight)
        cr.SetPen( pen )
        cr.DrawSpline( self.points )
        return

    def render( self, graphics, highlight=False ):
        graphics.set_pen( asggraphics.ASGGraphics.GraphicsRecordInt.pen_type_solid, self.pen.linewidth, asg_graphics_color(self.pen.color) )
        start = 0
        while start +3 < len(self.points):
            graphics.draw_bezier( self.points[start][0], self.points[start][1],
                                    self.points[start+1][0], self.points[start+1][1],
                                    self.points[start+2][0], self.points[start+2][1],
                                    self.points[start+3][0], self.points[start+3][1] )
            start += 3


class CompoundShape(Shape):

    def __init__(self, shapes):
        Shape.__init__(self)
        self.shapes = shapes

    def draw(self, cr, highlight=False):
        for shape in self.shapes:
            shape.draw(cr, highlight=highlight)

    def render(self, graphics, highlight=False):
        for shape in self.shapes:
            shape.render(graphics, highlight=highlight)


class Url(object):

    def __init__(self, item, url, highlight=None):
        self.item = item
        self.url = url
        if highlight is None:
            highlight = set([item])
        self.highlight = highlight


class Jump(object):

    def __init__(self, item, x, y, highlight=None):
        self.item = item
        self.x = x
        self.y = y
        if highlight is None:
            highlight = set([item])
        self.highlight = highlight


class Element(CompoundShape):
    """Base class for graph nodes and edges."""

    def __init__(self, shapes):
        CompoundShape.__init__(self, shapes)

    def get_url(self, x, y):
        return None

    def get_code_jump(self,x,y):
        return None

    def get_tooltip(self, x, y):
        return None

    def get_jump(self, x, y):
        return None


class Node(Element):

    def __init__(self, x, y, w, h, shapes, url, tooltip, code_jump = None ):
        Element.__init__(self, shapes)

        self.x = x
        self.y = y

        self.x1 = x - 0.5*w
        self.y1 = y - 0.5*h
        self.x2 = x + 0.5*w
        self.y2 = y + 0.5*h

        self.url = url
        self.tooltip = tooltip
        self.code_jump = code_jump

    def getText( self ):
        return '\n'.join( filter( None, [ shape.getText() for shape in self.shapes ] ) )

    def is_inside(self, x, y):
        #ret = self.x1 <= x and x <= self.x2 and self.y1 <= y and y <= self.y2
        #print 'is_inside:', (x,y), self, (self.x,self.y), ret
        return self.x1 <= x and x <= self.x2 and self.y1 <= y and y <= self.y2

    def get_url(self, x, y):
        if self.url is None:
            return None
        #print (x, y), (self.x1, self.y1), "-", (self.x2, self.y2)
        if self.is_inside(x, y):
            return Url(self, self.url)
        return None

    def get_code_jump(self, x, y):
        if self.code_jump is None or not self.is_inside(x,y):
            return None

        return self.code_jump

    def get_tooltip(self, x, y):
        if not self.tooltip or not self.is_inside(x, y):
            return None
        return self.tooltip

    def get_jump(self, x, y):
        if self.is_inside(x, y):
            return Jump(self, self.x, self.y)
        return None


def square_distance(x1, y1, x2, y2):
    deltax = x2 - x1
    deltay = y2 - y1
    return deltax*deltax + deltay*deltay


class Edge(Element):

    def __init__(self, src, dst, points, shapes):
        Element.__init__(self, shapes)
        self.src = src
        self.dst = dst
        self.points = points

    RADIUS = 10

    def get_jump(self, x, y):
        if square_distance(x, y, *self.points[0]) <= self.RADIUS*self.RADIUS:
            return Jump(self, self.dst.x, self.dst.y, highlight=set([self, self.dst]))
        if square_distance(x, y, *self.points[-1]) <= self.RADIUS*self.RADIUS:
            return Jump(self, self.src.x, self.src.y, highlight=set([self, self.src]))
        return None


class Graph(Shape):

    def __init__(self, width=1, height=1, shapes=(), nodes=(), edges=()):
        Shape.__init__(self)

        self.width = width
        self.height = height
        self.shapes = shapes
        self.nodes = nodes
        self.edges = edges

    def get_size(self):
        return self.width, self.height

    def draw(self, cr, highlight_items=None):
        if highlight_items is None:
            highlight_items = ()
        #cr.set_source_rgba(0.0, 0.0, 0.0, 1.0)

        #cr.set_line_cap(cairo.LINE_CAP_BUTT)
        #cr.set_line_join(cairo.LINE_JOIN_MITER)

        for shape in self.shapes:
            shape.draw(cr)
        for edge in self.edges:
            edge.draw(cr, highlight=(edge in highlight_items))
        for node in self.nodes:
            node.draw(cr, highlight=(node in highlight_items))

    def render(self, graphics, highlight_items=None):
        if highlight_items is None:
            highlight_items = ()

        for shape in self.shapes:
            shape.render(graphics)
        for node in self.nodes:
            node.render(graphics, highlight=(node in highlight_items))
        for edge in self.edges:
            edge.render(graphics, highlight=(edge in highlight_items))

    def get_url(self, x, y):
        for node in self.nodes:
            url = node.get_url(x, y)
            if url is not None:
                return url
        return None

    def get_jump(self, x, y):
        for edge in self.edges:
            jump = edge.get_jump(x, y)
            if jump is not None:
                return jump
        for node in self.nodes:
            jump = node.get_jump(x, y)
            if jump is not None:
                return jump
        return None

    def get_code_jump(self,x,y):
        for node in self.nodes:
            code_jump = node.get_code_jump(x, y)
            if code_jump is not None:
                return code_jump

        return None

class XDotAttrParser:
    """Parser for xdot drawing attributes.
    See also:
    - http://www.graphviz.org/doc/info/output.html#d:xdot
    """

    def __init__(self, parser, buf):
        self.parser = parser
        self.buf = self.unescape(buf)
        self.pos = 0

        self.pen = Pen()
        self.shapes = []

    def __nonzero__(self):
        return self.pos < len(self.buf)

    def unescape(self, buf):
        buf = buf.replace('\\"', '"')
        buf = buf.replace('\\n', '\n')
        return buf

    def read_code(self):
        pos = self.buf.find(" ", self.pos)
        res = self.buf[self.pos:pos]
        self.pos = pos + 1
        while self.pos < len(self.buf) and self.buf[self.pos].isspace():
            self.pos += 1
        return res

    def read_number(self):
        return int(self.read_code())

    def read_float(self):
        return float(self.read_code())

    def read_point(self):
        x = self.read_number()
        y = self.read_number()
        return self.transform(x, y)

    def read_text(self):
        num = self.read_number()
        pos = self.buf.find("-", self.pos) + 1
        self.pos = pos + num
        res = self.buf[pos:self.pos]
        while self.pos < len(self.buf) and self.buf[self.pos].isspace():
            self.pos += 1
        return res

    def read_polygon(self):
        n = self.read_number()
        p = []
        for _ in xrange(n):
            x, y = self.read_point()
            p.append((x, y))
        return p

    def read_color(self):
        # See http://www.graphviz.org/doc/info/attrs.html#k:color
        c = self.read_text()
        c1 = c[:1]
        if c1 == '#':
            hex2int = lambda h: int(h, 16)
            r = hex2int(c[1:3])
            g = hex2int(c[3:5])
            b = hex2int(c[5:7])
            try:
                a = hex2int(c[7:9])
            except (IndexError, ValueError):
                a = wx.ALPHA_OPAQUE
            return r, g, b, a
        elif c1.isdigit() or c1 == ".":
            # "H,S,V" or "H S V" or "H, S, V" or any other variation
            h, s, v = map(float, c.replace(",", " ").split())
            r, g, b = colorsys.hsv_to_rgb(h, s, v)
            a = 255
            return r, g, b, a
        else:
            return self.lookup_color(c)

    def lookup_color(self, c):
        color = wx.NamedColor(c)
        #s = 1.0/65535.0
        s = 1.
        r = color.Red()*s
        g = color.Green()*s
        b = color.Blue()*s
        a = wx.ALPHA_OPAQUE
        return r, g, b, a

    def parse(self):
        s = self

        while s:
            op = s.read_code()
            if op == "c":
                color = s.read_color()
                if color is not None:
                    self.handle_color(color, filled=False)
            elif op == "C":
                color = s.read_color()
                if color is not None:
                    self.handle_color(color, filled=True)
            elif op == "S":
                # http://www.graphviz.org/doc/info/attrs.html#k:style
                style = s.read_text()
                if style.startswith("setlinewidth("):
                    lw = style.split("(")[1].split(")")[0]
                    lw = float(lw)
                    self.handle_linewidth(lw)
                elif style in ("solid", "dashed"):
                    self.handle_linestyle(style)
            elif op == "F":
                size = s.read_float()
                name = s.read_text()
                self.handle_font(size, name)
            elif op == "T":
                x, y = s.read_point()
                j = s.read_number()
                w = s.read_number()
                t = s.read_text()
                self.handle_text(x, y, j, w, t)
            elif op == "E":
                x0, y0 = s.read_point()
                w = s.read_number()
                h = s.read_number()
                self.handle_ellipse(x0, y0, w, h, filled=True)
            elif op == "e":
                x0, y0 = s.read_point()
                w = s.read_number()
                h = s.read_number()
                self.handle_ellipse(x0, y0, w, h, filled=False)
            elif op == "L":
                points = self.read_polygon()
                self.handle_line(points)
            elif op == "B":
                points = self.read_polygon()
                self.handle_bezier(points, filled=False)
            elif op == "b":
                points = self.read_polygon()
                self.handle_bezier(points, filled=True)
            elif op == "P":
                points = self.read_polygon()
                self.handle_polygon(points, filled=True)
            elif op == "p":
                points = self.read_polygon()
                self.handle_polygon(points, filled=False)
            else:
                sys.stderr.write("unknown xdot opcode '%s'\n" % op)
                break

        return self.shapes

    def transform(self, x, y):
        return self.parser.transform(x, y)

    def handle_color(self, color, filled=False):
        if filled:
            self.pen.fillcolor = color
        else:
            self.pen.color = color

    def handle_linewidth(self, linewidth):
        self.pen.linewidth = linewidth

    def handle_linestyle(self, style):
        if style == "solid":
            self.pen.dash = ()
        elif style == "dashed":
            self.pen.dash = (6, )       # 6pt on, 6pt off

    def handle_font(self, size, name):
        #print 'handle_font:', name, size
        self.pen.fontsize = size - 2
        self.pen.fontname = name

    def handle_text(self, x, y, j, w, t):
        self.shapes.append(TextShape(self.pen, x, y, j, w, t))

    def handle_ellipse(self, x0, y0, w, h, filled=False):
        if filled:
            # xdot uses this to mean "draw a filled shape with an outline"
            self.shapes.append(EllipseShape(self.pen, x0, y0, w, h, filled=True))
        self.shapes.append(EllipseShape(self.pen, x0, y0, w, h))

    def handle_line(self, points):
        self.shapes.append(LineShape(self.pen, points))

    def handle_bezier(self, points, filled=False):
        if filled:
            # xdot uses this to mean "draw a filled shape with an outline"
            self.shapes.append(BezierShape(self.pen, points, filled=True))
        self.shapes.append(BezierShape(self.pen, points))

    def handle_polygon(self, points, filled=False):
        if filled:
            # xdot uses this to mean "draw a filled shape with an outline"
            self.shapes.append(PolygonShape(self.pen, points, filled=True))
        self.shapes.append(PolygonShape(self.pen, points))


EOF = -1
SKIP = -2


class ParseError(Exception):

    def __init__(self, msg=None, filename=None, line=None, col=None):
        self.msg = msg
        self.filename = filename
        self.line = line
        self.col = col

    def __str__(self):
        return ':'.join( str(part) for part in (self.filename, self.line, self.col, self.msg) if part != None )


class Scanner:
    """Stateless scanner."""

    # should be overriden by derived classes
    tokens = []
    symbols = {}
    literals = {}
    ignorecase = False

    def __init__(self):
        flags = re.DOTALL
        if self.ignorecase:
            flags |= re.IGNORECASE
        self.tokens_re = re.compile(
            '|'.join( '(' + regexp + ')' for _type, regexp, _test_lit in self.tokens ),
             flags
        )

    def next(self, buf, pos):
        if pos >= len(buf):
            return EOF, '', pos
        mo = self.tokens_re.match(buf, pos)
        if mo:
            text = mo.group()
            tokenType, _regexp, test_lit = self.tokens[mo.lastindex - 1]
            pos = mo.end()
            if test_lit:
                tokenType = self.literals.get(text, tokenType)
            return tokenType, text, pos
        else:
            c = buf[pos]
            return self.symbols.get(c, None), c, pos + 1


class Token:

    def __init__(self, type, text, line, col):
        self.type = type
        self.text = text
        self.line = line
        self.col = col


class Lexer:

    # should be overriden by derived classes
    scanner = None
    tabsize = 8

    newline_re = re.compile(r'\r\n?|\n')

    def __init__(self, buf = None, pos = 0, filename = None, fp = None):
        if fp is not None:
            try:
                fileno = fp.fileno()
                length = os.path.getsize(fp.name)
                import mmap
            except:
                # read whole file into memory
                buf = fp.read()
                pos = 0
            else:
                # map the whole file into memory
                if length:
                    # length must not be zero
                    buf = mmap.mmap(fileno, length, access = mmap.ACCESS_READ)
                    pos = os.lseek(fileno, 0, 1)
                else:
                    buf = ''
                    pos = 0

            if filename is None:
                try:
                    filename = fp.name
                except AttributeError:
                    filename = None

        self.buf = buf
        self.pos = pos
        self.line = 1
        self.col = 1
        self.filename = filename

    def next(self):
        while True:
            # save state
            pos = self.pos
            line = self.line
            col = self.col

            tokenType, text, endpos = self.scanner.next(self.buf, pos)
            assert pos + len(text) == endpos
            self.consume(text)
            tokenType, text = self.filter(tokenType, text)
            self.pos = endpos

            if tokenType == SKIP:
                continue
            elif tokenType is None:
                msg = 'unexpected char '
                if text >= ' ' and text <= '~':
                    msg += "'%s'" % text
                else:
                    msg += "0x%X" % ord(text)
                raise ParseError(msg, self.filename, line, col)
            else:
                break
        return Token(type = tokenType, text = text, line = line, col = col)

    def consume(self, text):
        # update line number
        pos = 0
        for mo in self.newline_re.finditer(text, pos):
            self.line += 1
            self.col = 1
            pos = mo.end()

        # update column number
        while True:
            tabpos = text.find('\t', pos)
            if tabpos == -1:
                break
            self.col += tabpos - pos
            self.col = ((self.col - 1)//self.tabsize + 1)*self.tabsize + 1
            pos = tabpos + 1
        self.col += len(text) - pos


class Parser:

    def __init__(self, lexer):
        self.lexer = lexer
        self.lookahead = self.lexer.next()

    def match(self, type):
        if self.lookahead.type != type:
            raise ParseError(
                msg = 'unexpected token %r' % self.lookahead.text,
                filename = self.lexer.filename,
                line = self.lookahead.line,
                col = self.lookahead.col)

    def skip(self, type):
        while self.lookahead.type != type:
            self.consume()

    def consume(self):
        token = self.lookahead
        self.lookahead = self.lexer.next()
        return token


ID = 0
STR_ID = 1
HTML_ID = 2
EDGE_OP = 3

LSQUARE = 4
RSQUARE = 5
LCURLY = 6
RCURLY = 7
COMMA = 8
COLON = 9
SEMI = 10
EQUAL = 11
PLUS = 12

STRICT = 13
GRAPH = 14
DIGRAPH = 15
NODE = 16
EDGE = 17
SUBGRAPH = 18


class DotScanner(Scanner):

    # token regular expression table
    tokens = [
        # whitespace and comments
        (SKIP,
            r'[ \t\f\r\n\v]+|'
            r'//[^\r\n]*|'
            r'/\*.*?\*/|'
            r'#[^\r\n]*',
        False),

        # Alphanumeric IDs
        (ID, r'[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*', True),

        # Numeric IDs
        (ID, r'-?(?:\.[0-9]+|[0-9]+(?:\.[0-9]*)?)', False),

        # String IDs
        (STR_ID, r'"[^"\\]*(?:\\.[^"\\]*)*"', False),

        # HTML IDs
        (HTML_ID, r'<[^<>]*(?:<[^<>]*>[^<>]*)*>', False),

        # Edge operators
        (EDGE_OP, r'-[>-]', False),
    ]

    # symbol table
    symbols = {
        '[': LSQUARE,
        ']': RSQUARE,
        '{': LCURLY,
        '}': RCURLY,
        ',': COMMA,
        ':': COLON,
        ';': SEMI,
        '=': EQUAL,
        '+': PLUS,
    }

    # literal table
    literals = {
        'strict': STRICT,
        'graph': GRAPH,
        'digraph': DIGRAPH,
        'node': NODE,
        'edge': EDGE,
        'subgraph': SUBGRAPH,
    }

    ignorecase = True


class DotLexer(Lexer):

    scanner = DotScanner()

    def filter(self, type, text):
        # TODO: handle charset
        if type == STR_ID:
            text = text[1:-1]

            # line continuations
            text = text.replace('\\\r\n', '')
            text = text.replace('\\\r', '')
            text = text.replace('\\\n', '')

            text = text.replace('\\r', '\r')
            text = text.replace('\\n', '\n')
            text = text.replace('\\t', '\t')
            text = text.replace('\\', '')

            type = ID

        elif type == HTML_ID:
            text = text[1:-1]
            type = ID

        return type, text


class DotParser(Parser):

    def __init__(self, lexer):
        Parser.__init__(self, lexer)
        self.graph_attrs = {}
        self.node_attrs = {}
        self.edge_attrs = {}

    def parse(self):
        self.parse_graph()
        self.match(EOF)

    def parse_graph(self):
        if self.lookahead.type == STRICT:
            self.consume()
        self.skip(LCURLY)
        self.consume()
        while self.lookahead.type != RCURLY:
            self.parse_stmt()
        self.consume()

    def parse_subgraph(self):
        nodeId = None
        if self.lookahead.type == SUBGRAPH:
            self.consume()
            if self.lookahead.type == ID:
                nodeId = self.lookahead.text
                self.consume()
        if self.lookahead.type == LCURLY:
            self.consume()
            while self.lookahead.type != RCURLY:
                self.parse_stmt()
            self.consume()
        return nodeId

    def parse_stmt(self):
        if self.lookahead.type == GRAPH:
            self.consume()
            attrs = self.parse_attrs()
            self.graph_attrs.update(attrs)
            self.handle_graph(attrs)
        elif self.lookahead.type == NODE:
            self.consume()
            self.node_attrs.update(self.parse_attrs())
        elif self.lookahead.type == EDGE:
            self.consume()
            self.edge_attrs.update(self.parse_attrs())
        elif self.lookahead.type in (SUBGRAPH, LCURLY):
            self.parse_subgraph()
        else:
            nodeId = self.parse_node_id()
            if self.lookahead.type == EDGE_OP:
                self.consume()
                node_ids = [nodeId, self.parse_node_id()]
                while self.lookahead.type == EDGE_OP:
                    node_ids.append(self.parse_node_id())
                attrs = self.parse_attrs()
                for i in xrange(0, len(node_ids) - 1):
                    self.handle_edge(node_ids[i], node_ids[i + 1], attrs)
            elif self.lookahead.type == EQUAL:
                self.consume()
                self.parse_id()
            else:
                attrs = self.parse_attrs()
                self.handle_node(nodeId, attrs)
        if self.lookahead.type == SEMI:
            self.consume()

    def parse_attrs(self):
        attrs = {}
        while self.lookahead.type == LSQUARE:
            self.consume()
            while self.lookahead.type != RSQUARE:
                name, value = self.parse_attr()
                attrs[name] = value
                if self.lookahead.type == COMMA:
                    self.consume()
            self.consume()

        return attrs

    def parse_attr(self):
        name = self.parse_id()

        if self.lookahead.type == EQUAL:
            self.consume()
            value = self.parse_id()
        else:
            value = 'true'
        return name, value

    def parse_node_id(self):
        node_id = self.parse_id()
        if self.lookahead.type == COLON:
            self.consume()
            _port = self.parse_id()
            if self.lookahead.type == COLON:
                self.consume()
                ### Must call this as it has a side effect of consume further data.
                _compass_pt = self.parse_id()

        return node_id

    def parse_id(self):
        self.match(ID)
        nodeId = self.lookahead.text
        self.consume()
        return nodeId

    def handle_graph(self, attrs):
        pass

    def handle_node(self, id, attrs):
        pass

    def handle_edge(self, src_id, dst_id, attrs):
        pass


class XDotParser(DotParser):

    def __init__(self, xdotcode):
        lexer = DotLexer(buf = xdotcode)
        DotParser.__init__(self, lexer)

        self.nodes = []
        self.edges = []
        self.shapes = []
        self.node_by_name = {}
        self.top_graph = True

    @staticmethod
    def encodeCodeJump( source_file, line_number ):
        '''encode to string
        source_file should be full physical path, ie '/HYDRA/(ps)/tspimpl/wirerun.py'
        For some reason the lexer replaces all \\ with blanks, rather than impact the world, do our own encoding
        on the filename here'''
        source_file = source_file.replace( "\\", '%SLASH%' )
        return "::".join( [ source_file, str( line_number ) ] )

    @staticmethod
    def decodeCodeJump( code_jump ):
        ''' from a CodeJump value, returns a tuple ( source_file, line_number ) '''
        filename, line = code_jump.split( "::" )
        filename = filename.replace( '%SLASH%', "\\" )
        line = int( line )
        return ( filename, line )

    def handle_graph(self, attrs):
        if self.top_graph:
            try:
                bb = attrs['bb']
            except KeyError:
                return

            if not bb:
                return

            xmin, ymin, xmax, ymax = map(float, bb.split(","))

            #print 'graph:', xmin, ymin, xmax, ymax
            self.xoffset = -xmin
            self.yoffset = -ymax
            self.xscale = 1.0
            self.yscale = -1.0
            # FIXME: scale from points to pixels

            self.width = xmax - xmin
            self.height = ymax - ymin

            self.top_graph = False

        for attr in ("_draw_", "_ldraw_", "_hdraw_", "_tdraw_", "_hldraw_", "_tldraw_"):
            if attr in attrs:
                parser = XDotAttrParser(self, attrs[attr])
                self.shapes.extend(parser.parse())

    def handle_node(self, id, attrs):
        try:
            pos = attrs['pos']
        except KeyError:
            return

        x, y = self.parse_node_pos(pos)
        w = float(attrs['width'])*72
        h = float(attrs['height'])*72
        shapes = []
        for attr in ("_draw_", "_ldraw_"):
            if attr in attrs:
                parser = XDotAttrParser(self, attrs[attr])
                shapes.extend(parser.parse())
        url = attrs.get('URL', None)
        tooltip = attrs.get('tooltip', None)

        code_jump = attrs.get('CodeJump', None)
        if code_jump is not None:
            code_jump = self.decodeCodeJump( code_jump )

        node = Node(x, y, w, h, shapes, url, tooltip,code_jump)
        self.node_by_name[id] = node
        if shapes:
            self.nodes.append(node)

    def handle_edge(self, src_id, dst_id, attrs):
        try:
            pos = attrs['pos']
        except KeyError:
            return

        points = self.parse_edge_pos(pos)
        shapes = []
        for attr in ("_draw_", "_ldraw_", "_hdraw_", "_tdraw_", "_hldraw_", "_tldraw_"):
            if attr in attrs:
                parser = XDotAttrParser(self, attrs[attr])
                shapes.extend(parser.parse())
        if shapes:
            src = self.node_by_name[src_id]
            dst = self.node_by_name[dst_id]
            self.edges.append(Edge(src, dst, points, shapes))

    def parse(self):
        DotParser.parse(self)

        return Graph(self.width, self.height, self.shapes, self.nodes, self.edges)

    def parse_node_pos(self, pos):
        x, y = pos.split(",")
        return self.transform(float(x), float(y))

    def parse_edge_pos(self, pos):
        points = []
        for entry in pos.split(' '):
            fields = entry.split(',')
            try:
                x, y = fields
            except ValueError:
                # TODO: handle start/end points
                continue
            else:
                points.append(self.transform(float(x), float(y)))
        return points

    def transform(self, x, y):
        # XXX: this is not the right place for this code
        x = (x + self.xoffset)*self.xscale
        y = (y + self.yoffset)*self.yscale
        return x, y


class Animation(object):

    step = 0.03 # seconds

    def __init__(self, dot_widget):
        self.dot_widget = dot_widget
        self.timeout_id = None
        self.running = False

    def start(self):
        self.running = True
        #self.timeout_id = gobject.timeout_add(int(self.step * 1000), self.tick)
        wx.CallLater( 1000*self.step, self._tick )

    def _tick(self):
        if self.tick():
            wx.CallLater( 1000*self.step, self._tick )


    def stop(self):
        self.dot_widget.animation = NoAnimation(self.dot_widget)
        self.running = False

    def tick(self):
        self.stop()


class NoAnimation(Animation):

    def start(self):
        pass

    def stop(self):
        pass



class LinearAnimation(Animation):

    duration = 0.6

    def start(self):
        self.started = time.time()
        Animation.start(self)

    def tick(self):
        t = (time.time() - self.started) / self.duration
        self.animate(max(0, min(t, 1)))
        return (t < 1)

    def animate(self, t):
        pass


class MoveToAnimation(LinearAnimation):

    def __init__(self, dot_widget, target_x, target_y):
        Animation.__init__(self, dot_widget)
        self.source_x = dot_widget.x
        self.source_y = dot_widget.y
        self.target_x = target_x
        self.target_y = target_y

    def animate(self, t):
        sx, sy = self.source_x, self.source_y
        tx, ty = self.target_x, self.target_y
        ## this makes the animation start out fast and then slow down as we approach the target
        t = t**.3
        x = sx*(1-t) + tx*t
        y = sy*(1-t) + ty*t
        self.dot_widget.y = y
        self.dot_widget.x = x
        self.dot_widget.queue_draw()


class ZoomToAnimation(MoveToAnimation):

    def __init__(self, dot_widget, target_x, target_y):
        MoveToAnimation.__init__(self, dot_widget, target_x, target_y)
        self.source_zoom = dot_widget.zoom_ratio
        self.target_zoom = self.source_zoom
        self.extra_zoom = 0

        middle_zoom = 0.5 * (self.source_zoom + self.target_zoom)

        distance = math.hypot(self.source_x - self.target_x,
                              self.source_y - self.target_y)
        rect = self.dot_widget.GetRect()
        visible = min(rect.width, rect.height) / self.dot_widget.zoom_ratio
        visible *= 0.9
        if distance > 0:
            desired_middle_zoom = visible / distance
            self.extra_zoom = min(0, 4 * (desired_middle_zoom - middle_zoom))

    def animate(self, t):
        a, b, c = self.source_zoom, self.extra_zoom, self.target_zoom
        self.dot_widget.zoom_ratio = c*t + b*t*(1-t) + a*(1-t)
        self.dot_widget.zoom_to_fit_on_resize = False
        MoveToAnimation.animate(self, t)


class DragAction(object):

    def __init__(self, dot_widget):
        self.dot_widget = dot_widget

    def on_button_press(self, event):
        self.startmousex = self.prevmousex = event.GetX()
        self.startmousey = self.prevmousey = event.GetY()
        self.start()

    def on_motion_notify(self, event):
        deltax = self.prevmousex - event.GetX()
        deltay = self.prevmousey - event.GetY()
        deltax = 2*deltax
        deltay = 2*deltay
        self.drag(deltax, deltay)
        self.prevmousex = event.GetX()
        self.prevmousey = event.GetY()
        self.dot_widget.Refresh()

    def on_button_release(self, event):
        self.stopmousex = event.GetX()
        self.stopmousey = event.GetY()
        self.stop()

    def draw(self, cr):
        pass

    def start(self):
        pass

    def drag(self, deltax, deltay):
        pass

    def stop(self):
        pass

    def abort(self):
        pass


class NullAction(DragAction):

    def on_motion_notify(self, event):
        dot_widget = self.dot_widget
        item = dot_widget.get_url(event.GetX(), event.GetY())
        if item is None:
            item = dot_widget.get_jump(event.GetX(), event.GetY())
        if item is not None:
            dot_widget.SetCursor( wx.StockCursor( wx.CURSOR_HAND ) )
            dot_widget.set_highlight(item.highlight)
        else:
            dot_widget.SetCursor( wx.StockCursor( wx.CURSOR_ARROW ) )
            dot_widget.set_highlight(None)


class PanAction(DragAction):

    def start(self):
        #self.dot_widget.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.FLEUR))
        self.dot_widget.SetCursor( wx.StockCursor( wx.CURSOR_SIZING ) )

    def drag(self, deltax, deltay):
        self.dot_widget.x += deltax / self.dot_widget.zoom_ratio
        self.dot_widget.y += deltay / self.dot_widget.zoom_ratio
        self.dot_widget.queue_draw()

    def stop(self):
        self.dot_widget.SetCursor( wx.StockCursor( wx.CURSOR_ARROW ) )

    abort = stop


class ZoomAction(DragAction):

    def drag(self, deltax, deltay):
        self.dot_widget.zoom_ratio *= 1.005 ** (deltax + deltay)
        self.dot_widget.zoom_to_fit_on_resize = False
        self.dot_widget.queue_draw()

    def stop(self):
        self.dot_widget.queue_draw()


class ZoomAreaAction(DragAction):

    def drag(self, deltax, deltay):
        self.dot_widget.queue_draw()

    def draw(self, cr):
        cr.save()
        cr.set_source_rgba(.5, .5, 1.0, 0.25)
        cr.rectangle(self.startmousex, self.startmousey,
                     self.prevmousex - self.startmousex,
                     self.prevmousey - self.startmousey)
        cr.fill()
        cr.set_source_rgba(.5, .5, 1.0, 1.0)
        cr.set_line_width(1)
        cr.rectangle(self.startmousex - .5, self.startmousey - .5,
                     self.prevmousex - self.startmousex + 1,
                     self.prevmousey - self.startmousey + 1)
        cr.stroke()
        cr.restore()

    def stop(self):
        x1, y1 = self.dot_widget.window2graph(self.startmousex,
                                              self.startmousey)
        x2, y2 = self.dot_widget.window2graph(self.stopmousex,
                                              self.stopmousey)
        self.dot_widget.zoom_to_area(x1, y1, x2, y2)

    def abort(self):
        self.dot_widget.queue_draw()


def _get_chart_pointer(chart):
    """ Parse and return the C++ pointer information for a chart.

    Parameters
    ----------
    chart : pychartdir.AutoMethod
        The Chart Director object representing a chart.

    Returns
    -------
    result : (string, int)
        A 2-tuple of pointer information. The string is the name of the
        C++ chart class. The int is the pointer address of the class.

    Raises
    ------
    TypeError
        The chart is of the incorrect type.

    ValueError
        The function is unable to parse the pointer information.

    """
    ptr = pychartdir.decodePtr( chart )
    if not isinstance(ptr, basestring):
        name = type(chart).__name__
        raise TypeError("Can't get pointer for chart of type `%s`" % name)
    match = _NULL_PTR_RE.match(ptr)
    if match:
        return ('void', 0)
    match = _CLASS_PTR_RE.match(ptr)
    if match:
        name, addr = match.groups()
        return (name, int(addr, 16))
    raise ValueError('Bad pointer `%s`' % ptr)

class ChartSource(object):
    """ A base class for defining charts.

    Subclasses must implement the `create_chart` method.

    """
    def __init__(self):
        self._second_format = '{value|hh:nn:ss}'
        self._subsecond_format = '{value|hh:nn:ss.fff}'
        self._date_time_format = '{value|dd mmm yyyy}'
        self._date_time_time_format = '{value|dd mmm yyyy hh:nn:ss}'
        self._date_time_detail_format = '{value|dd mmm yyyy hh:nn:ss.fff}'

        self._mouse_track_second_format = 'hh:nn:ss'
        self._mouse_track_subsecond_format = 'hh:nn:ss.fff'
        self._mouse_track_date_time_format = 'dd mmm yyyy'
        self._mouse_track_date_time_time_format = 'dd mmm yyyy hh:nn:ss'
        self._mouse_track_date_time_detail_format = 'dd mmm yyyy hh:nn:ss.fff'

    def _get_ptr(self, chart):
        """ A private method called from C++.

        """
        return _get_chart_pointer(chart)

    def create_graphics(self):
        """ Reimplement this method to display ASGGraphics (as ui.xdot now does)

        Returns
        -------
        result : ASGGraphics.get_data()
        """
        pass

    def create_chart(self, width, height, zoom_offset, zoom_extent, vector=False):
        """ Reimplement this method to generate your chart.

        Parameters
        ----------
        width : int
            The width of the chart, in pixels.

        height : int
            The height of the chart, in pixels.

        zoom_offset : float
            The horizontal offset of the zoomed area. This is a percentage
            in the range 0.0 to 1.0. A value 0.5 indicate that the left
            edge of the zoom area is at the middle of the data space.

        zoom_extent : float
            The extent of the zoomed chart area. This is a percentage in
            the range of 0.0 to 1.0. A value of 0.5 indicates that 50%
            of the data space is viewable.

        vector : bool, optional
            Whether or not to enable vector output on the chart.

        Returns
        -------
        result : XYChart
            A pychartdir XYChart instance.

        """
        pass

    def get_mouseover_labels( self ):
        ''' Return the definition of labels to show on mouse-over action
            Each element can be String or Tuple (ASGGraphics, width, height)
        '''
        return []

    def set_chart_view(self, chartView):
        ''' Set the chart view gui component to draw onto '''
        self._chartView = chartView

    def get_linked_cells(self, chart, x, y):
        """ An asggrid helper to show linked cells.

        When a chart is embedded in an asggrid, this method can provide
        a list of cells to show as linked to the point x, y

        """
        return None

    def button_clicked(self, x, y):
        """ The listener for the button click event.

        Parameters
        ----------
        pixel : 2-tuple of ints
            The (x, y) pixel position of the mouse.

        """
        pass

    def zoom_slice(self, data, zoom_offset, zoom_extent):
        """ A helper method for taking a zoomed slice of a data set.

        Parameters
        ----------
        data : sequence
            A sequence of data from which to take a slice.

        zoom_offset : float
            The zoom offset for the slice.

        zoom_extent : float
            The zoom extent for the slice.

        Returns
        -------
        result : sequence
            The subset of the original sequence for the zoomed slice. If
            the zoom extent is >= 1.0, the original sequence is returned.

        """
        if zoom_extent >= 1.0:
            return data
        data_len   = len(data)
        zoom_start = int( math.floor( data_len * zoom_offset) )
        zoom_end   = int( math.ceil( zoom_start + data_len * zoom_extent) )
        return data[zoom_start:zoom_end]

    def find_slice(self, data, zoom_offset, zoom_extent):
        """ A helper method for taking a zoomed slice of a data set.

        Parameters
        ----------
        data : sequence
            A *sorted* sequence of data from which to take a slice.

        zoom_offset : float
            The zoom offset for the slice.

        zoom_extent : float
            The zoom extent for the slice.

        Returns
        -------
        result : tuple
            (slice start, slice end)

        """
        data_len = len(data)
        if zoom_extent >= 1.0:
            return (0, data_len-1)
        data_range = data[-1] - data[0]
        left_value = data[0] + ( data_range * zoom_offset )
        right_value = left_value + ( data_range * zoom_extent )
        left = bisect.bisect_left( data, left_value )
        right = bisect.bisect_right( data, right_value, left )
        return left, min(data_len-1,right)

    def get_time_format(self, scale_range):
        """ Return a format string suitable for date/time scales
        to be used on ChartDirector x axis """
        if scale_range > 24 * 3600 *3:
            label_format = self._date_time_format
        elif scale_range > 24 * 3600:
            label_format = self._date_time_time_format
        elif scale_range > 20:
            label_format = self._second_format
        else:
            label_format = self._subsecond_format
        return label_format

    def get_mouse_track_time_format(self, scale_range):
        """ Return a format string suitable for date/time scales
        to be with get_mouse_track_label_format() """
        if scale_range > 24 * 3600 *3:
            label_format = self._mouse_track_date_time_format
        elif scale_range > 24 * 3600:
            label_format = self._mouse_track_date_time_time_format
        elif scale_range > 20:
            label_format = self._mouse_track_second_format
        else:
            label_format = self._mouse_track_subsecond_format
        return label_format

    def to_svg(self, width, height, name=""):
        """ Create a pdf output of the chart. If name is empty, a temporary file
            will be created. The name of the file will be returned.
        """
        fd = None
        if not name:
            fd, name = tempfile.mkstemp(suffix='.svg', text=True)

        chart = self.create_chart(width, height, 0, 1, True)
        svg = chart.makeChart2(pychartdir.SVG)
        with open(name, 'wb') as svg_file:
            svg_file.write(svg)

        if fd:
            os.close(fd)

        return name

    def to_pdf(self, width, height, name=""):
        """ Create a pdf output of the chart. If name is empty, a temporary file
            will be created. Suffix must be '.pdf'. The name of the file will be returned.
        """
        if name.endswith('.pdf'):
            svg_name = name[:-4] + '.svg'
        else:
            svg_name = name + '.svg'

        svg_name = self.to_svg(width, height, svg_name)
        name = svg_name[:-4] + '.pdf'
        batik_jar = '%s/batik-rasterizer.jar' % os.environ['BATIK_CP_HOME']
        if 'win32' == sys.platform:
            import ASGWinUtil # Prevent the cmd window from popping up
            ASGWinUtil.createProcess( ['java', '-jar', batik_jar, '-m', 'application/pdf', svg_name] )
        else:
            os.system('java -jar %s -m application/pdf %s' % ( batik_jar, svg_name ) )
        os.remove(svg_name)
        return name

    def to_png(self, width, height, name=""):
        """ Create a png output of the chart. If name is empty, a temporary file
            will be created. The name of the file will be returned.
        """
        fd = None
        if not name:
            fd, name = tempfile.mkstemp(suffix='.png', text=False)

        chart = self.create_chart(width, height, 0, 1, False)
        png = chart.makeChart2(pychartdir.PNG)
        with open(name, 'wb') as png_file:
            png_file.write(png)

        if fd:
            os.close(fd)

        return name

    def to_emf(self, width, height, name=""):
        """ Create a emf output of the chart. If name is empty, a temporary file
            will be created. Suffix must be .emf. The name of the file will be returned.
        """
        fd = None
        if not name:
            fd, name = tempfile.mkstemp(suffix='.emf', text=False)

        if name.endswith('.emf'):
            svg_name = name[:-4] + '.svg'
        else:
            svg_name = name + '.svg'

        name = svg_name[:-4] + '.emf'

        if not sys.platform == 'win32':
            t = 'EMF format cannot be generated on this platform: %s'
            raise RuntimeError(t % sys.platform)

        chart = self.create_chart(width, height, 0, 1, True)
        svg = chart.makeChart2(pychartdir.SVG)
        svg_tag_index = svg.find('<svg ')

        if svg_tag_index > 0:
            svg_tag = '<svg width="%d" height="%d"' % (width, height)
            svg = " ".join((svg[0:svg_tag_index], svg_tag, svg[svg_tag_index+4:]))

        with open(svg_name, 'wb') as svg_file:
            svg_file.write(svg)

        cmd = 'svg2emf file:///%s %s %d %d'
        os.system(cmd % (svg_name, name , width, height))
        os.remove(svg_name)

        if fd:
            os.close(fd)

        return name

    def invalidate(self):
        """ Tell the chart widget to repaint itself, which will eventually trigger
        a call to create_chart.
        """
        if getattr(self, '_chartView', None):
            self._chartView.repaint()

    def repaint_later(self, delay_ms):
        """ Thread-safe API to trigger repaint with a delay """
        self._chartView.repaint_later( delay_ms )

    def show_wait_msg(self):
        """ With enough points, the chart may take some time to render.
            Return True here to show a wait message to prevent a window that appears hung
        """
        return False

    def get_mouse_track_label_format(self):
        """ Optionally provide an x axis format string for interactive mouse track """
        return ''

    def get_clipboard_icon_pos(self):
        """ Returns
            -------
            True: show the clipboard icon using the default position
            Tuple (int,int): show the clipboard icon using the position supplied
            None (or anything else): don't show the clipboard icon
        """
        return True

class DotChartSource(ChartSource):
    def __init__(self):
        self.filter = 'dot'
        self.graph = Graph()
        self.openfilename = None
        self.tooltips = None

    def set_xdotcode(self, xdotcode):
        parser = XDotParser(xdotcode)

        self.graph = parser.parse()

    def set_dotcode(self, dotcode, filename='<stdin>'):
        if isinstance(dotcode, unicode):
            dotcode = dotcode.encode('utf8')
        p = subprocess.Popen(
            [self.filter, '-Txdot'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=False,
            universal_newlines=True
        )
        xdotcode, error = p.communicate(dotcode)
        if p.returncode != 0:
            import dialog
            dialog.ErrorWarning( error, 'Dot Viewer' )
        try:
            self.set_xdotcode(xdotcode)
        except ParseError, ex:
            import dialog
            dialog.ErrorWarning( str(ex), 'Dot Viewer' )
            return False
        else:
            self.openfilename = filename
            return True

    def render(self):
        self.g = asggraphics.ASGGraphics.GraphicsRecordInt()
        self.g.set_pen( asggraphics.ASGGraphics.GraphicsRecordInt.pen_type_solid, 1, 0xFFFFFFFF )
        self.g.set_brush( asggraphics.ASGGraphics.GraphicsRecordInt.brush_type_solid, 0xFFFFFFFF )
        self.g.fill_background()
        self.g.set_font( 'Arial', 9, asggraphics.ASGWidget.FontStyleRegular, 0 )
        self.graph.render( self.g, highlight_items=() )

    def create_graphics(self):
        return self.g

    def get_mouseover_labels( self ):
        if self.tooltips is None:
            self.tooltips = []
            for node in self.graph.nodes:
                self.tooltips.append((int(node.x1), int(node.y1), int(node.x2), int(node.y2),node.tooltip))
        return self.tooltips

    def get_clipboard_icon_pos(self):
        return None

    def button_clicked(self, x, y):
        code_jump = self.graph.get_code_jump(x, y)
        #if code_jump and pie.shared.pieFrame:
        #    pie.shared.pieFrame.editorNotebook.SetCallingLineMarker( code_jump[0], int( code_jump[1] ) )


class DotWidget(wx.PyControl):
    ID_ZOOM_IN = wx.NewId()
    ID_ZOOM_OUT = wx.NewId()
    ID_ZOOM_100 = wx.NewId()
    ID_CENTER = wx.NewId()
    ID_FIT = wx.NewId()

    def __init__(self, parent):
        wx.PyControl.__init__(self, parent)

        self.filter = 'dot'
        self.graph = Graph()
        self.openfilename = None
        self.buffer = wx.EmptyBitmap( 1, 1 )
        self.winbuffer = wx.EmptyBitmap( *self.GetSize().Get() )

        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind( wx.EVT_IDLE, self.OnIdle )
        self.Bind( wx.EVT_RIGHT_DOWN, self.OnRightDown )
        self.Bind( wx.EVT_MOUSEWHEEL, self.OnWheel )

        self.legendFont = wx.FFont( 7, wx.FONTFAMILY_SWISS )
        self.hoverFont = wx.FFont( 9, wx.FONTFAMILY_SWISS )
        self.legendBrush = wx.Brush( wx.Colour( 254, 254, 254, wx.ALPHA_OPAQUE ) )
        self.hoverBrush = wx.Brush( wx.SystemSettings_GetColour( wx.SYS_COLOUR_INFOBK ) )

        self.Bind( wx.EVT_KEY_DOWN, self.on_key_press_event )
        self.Bind( wx.EVT_SIZE, self.OnSize )
        self.Bind( wx.EVT_LEFT_DOWN, self.on_area_button_press )
        self.Bind( wx.EVT_LEFT_UP, self.on_area_button_release )
        self.Bind( wx.EVT_MOTION, self.on_area_motion_notify )

        self.Bind( wx.EVT_MENU, lambda evt: self.on_key_press_event( kc=ord( '=' ) ), id=self.ID_ZOOM_IN )
        self.Bind( wx.EVT_MENU, lambda evt: self.on_key_press_event( kc=ord( '-' ) ), id=self.ID_ZOOM_OUT )
        self.Bind( wx.EVT_MENU, lambda evt: self.on_key_press_event( kc=ord( '0' ) ), id=self.ID_ZOOM_100 )
        self.Bind( wx.EVT_MENU, lambda evt: self.center_image(), id=self.ID_CENTER )
        self.Bind( wx.EVT_MENU, lambda evt: self.zoom_to_fit(), id=self.ID_FIT )

        self.x, self.y = 0.0, 0.0
        self.last_x, self.last_y = None, None
        self.zoom_ratio = 1.0
        self.last_zoom = None
        self.zoom_to_fit_on_resize = False
        self.animation = NoAnimation(self)
        self.drag_action = NullAction(self)
        self.highlight = None
        self.SetBackgroundStyle( wx.BG_STYLE_CUSTOM )
        self.mousepos = (0,0)
        self.mouseposT = sys.maxint
        self.hovertxt = ''
        self.dragging = False

    def OnWheel( self, evt ):
        scroll = evt.GetWheelRotation()
        inc = -scroll/self.zoom_ratio
        self.y += inc
        self.queue_draw()

    def OnSize( self, evt ):
        self.winbuffer = wx.EmptyBitmap( *evt.GetSize().Get() )
        self.Refresh()

    def OnRightDown( self, evt ):
        menu = wx.Menu()
        menu.Append( self.ID_CENTER, 'Center\tC' )
        menu.Append( self.ID_FIT, 'Fit\tF' )
        menu.AppendSeparator()
        menu.Append( self.ID_ZOOM_IN, 'Zoom In\t=' )
        menu.Append( self.ID_ZOOM_OUT, 'Zoom Out\t-' )
        menu.Append( self.ID_ZOOM_100, 'Zoom 100%\t0' )
        self.PopupMenu( menu )

    def OnIdle( self, evt ):
        if self.dragging:
            return
        x, y = wx.GetMousePosition().Get()
        t = time.time()
        if self.mousepos == (x,y):
            if not self.hovertxt and t-self.mouseposT > .25:
                x, y = self.ScreenToClient( self.mousepos ).Get()
                x, y = self.window2graph(x + self.x, y + self.y)
                jump = self.graph.get_jump( x, y )
                if jump:
                    tip = jump.item.get_tooltip( x, y )
                    if tip:
                        self.hovertxt = tip
                    elif self.zoom_ratio < 1:
                        self.hovertxt = jump.item.getText()
                if self.hovertxt:
                    self.Refresh()
        else:
            if self.hovertxt:
                self.hovertxt = ''
                self.Refresh()
            self.mouseposT = t
            self.mousepos = (x,y)


    def queue_draw( self, force=False ):
        size = self.GetSize()
        self.x = min( self.x, self.zoom_ratio*(self.graph.width) - size.width*.5 )
        self.y = min( self.y, self.zoom_ratio*(self.graph.height) - size.height*.5 )

        self.x = max( -size.width/2, self.x )
        self.y = max( -size.width/2, self.y )

        self.x = int( self.x )
        self.y = int( self.y )

        if self.last_zoom != self.zoom_ratio or force:
            self.doPaint()

        if (self.last_x, self.last_y, self.last_zoom) != (self.x, self.y, self.zoom_ratio) or force:
            self.hovertxt = ''
            self.Refresh()

        self.last_zoom = self.zoom_ratio
        self.last_x = self.x
        self.last_y = self.y


    def OnPaint(self, evt):
        try:
            dc = wx.MemoryDC( self.winbuffer )
            dc.SetBackground( wx.WHITE_BRUSH )
            dc.Clear()
            dc = wx.GCDC( dc )

            tmpdc = wx.MemoryDC( self.buffer )
            size = self.GetSize()
            x, y = self.x, self.y

            bufx, bufy = self.buffer.GetSize().Get()
            w = min( size.width, bufx-x )
            h = min( size.height, bufy-y )
            xoff, yoff = 0, 0

            dc.SetBrush( wx.WHITE_BRUSH )
            dc.SetPen( wx.TRANSPARENT_PEN )
            if x < 0:
                xoff = abs( x )
                dc.DrawRectangle( 0, 0, xoff, h )
                x = 0
            if y < 0:
                yoff = abs( y )
                dc.DrawRectangle( 0, 0, w, yoff )
                y = 0

            dc.Blit( xoff, yoff, w, h, tmpdc, x, y )

            tmpdc.SelectObject( wx.NullBitmap )

            ## Draw text in UL corner ##
            dc.SetTextForeground(wx.BLACK)
            dc.SetFont( self.legendFont )
            dc.SetBrush( self.legendBrush )
            dc.SetPen( wx.TRANSPARENT_PEN )
            rect = wx.Rect( 2, 2, 70, 20 )
            dc.DrawLabel( 'Zoom %i%%' % ( 100*self.zoom_ratio, ), rect )

            if self.hovertxt:
                dc.SetFont( self.hoverFont )
                hx, hy = self.ScreenToClient( self.mousepos ).Get()
                hx += 10
                hy -= 10
                hw = 0
                hh = 0
                for line in self.hovertxt.splitlines():
                    hwl, hhl = dc.GetTextExtent( line )
                    hw = max( hw, hwl )
                    hh += hhl
                hy -= (hh+10)
                hx -= max( 0, (hx+hw+10)-size.width )
                hy = max( 10, hy )

                dc.SetBrush( wx.Brush( wx.Colour( 0, 0, 0, 64 ) ) )
                dc.SetPen( wx.TRANSPARENT_PEN )
                dc.DrawRoundedRectangle( hx+3, hy+3, hw+8, hh+8, 4 )

                dc.SetBrush( self.hoverBrush )
                dc.SetPen( wx.Pen( wx.Colour( 0x99, 0x99, 0x99 ) ) )
                dc.DrawRoundedRectangle( hx, hy, hw+8, hh+8, 4 )

                rect.Deflate( 4, 4 )
                rect = wx.Rect( hx, hy, hw+8, hh+8 )
                dc.DrawLabel( self.hovertxt, rect, wx.ALIGN_CENTER )

            ## Draw overview in BR ##
            self.paintOverview( dc )

            pdc = wx.PaintDC( self )

            tmpdc = wx.MemoryDC( self.winbuffer )
            pdc.Blit( 0, 0, size.width, size.height, tmpdc, 0, 0 )

        except:
            traceback.print_exc()
            self.GetTopLevelParent().Destroy()

    def paintOverview( self, dc ):
        size = self.GetSize()
        pen = wx.Pen( wx.Colour( 0x44, 0x44, 0x44 ) )
        pen.SetWidth( 2 )
        dc.SetPen( pen )
        ow = self.overview.GetSize().width
        oh = self.overview.GetSize().height
        ox = size.width - ow - 3
        oy = size.height - oh - 3
        dc.DrawRectangle( ox, oy, ow+2, oh+2 )
        tmpdc = wx.MemoryDC( self.overview )
        dc.Blit( ox+1, oy+1, ow, oh, tmpdc, 0, 0 )
        tmpdc.SelectObject( wx.NullBitmap )

        ## draw overview region box ##
        pen = wx.Pen( wx.Colour( 0x33, 0x33, 0xee ) )
        dc.SetPen( pen )
        dc.SetBrush( wx.Brush( wx.Colour( 0x33, 0x33, 0xee, 64 ) ) )
        zx = ox + 1 + (self.x/self.graph.width) * ow / self.zoom_ratio
        zy = oy + 1 + (self.y/self.graph.height) * oh / self.zoom_ratio

        zw = ow * size.width / self.graph.width / self.zoom_ratio
        zh = oh * size.height / self.graph.height / self.zoom_ratio
        dc.SetClippingRegion( ox, oy, ow, oh )
        dc.DrawRectangle( zx, zy, zw, zh )
        dc.DestroyClippingRegion()

    def doPaint( self ):
        ## unbind so key presses while busy drawing don't get used
        self.Unbind( wx.EVT_KEY_DOWN )

        bx, by = self.buffer.GetSize()
        if bx < self.zoom_ratio*self.graph.width or by < self.zoom_ratio*self.graph.height:
            # print 'new buffer:', self.zoom_ratio*self.graph.width, self.zoom_ratio*self.graph.height
            self.buffer = wx.EmptyBitmap( self.zoom_ratio*self.graph.width, self.zoom_ratio*self.graph.height, )

        mdc = wx.MemoryDC( self.buffer )
        dc = wx.GCDC( mdc )
        dc = mdc

        dc.SetUserScale( self.zoom_ratio, self.zoom_ratio )
        self.draw( dc )

        dc.SetUserScale( 1.0, 1.0 )
        self.drag_action.draw( dc )

        mdc.SelectObject( wx.NullBitmap )

        self.Bind( wx.EVT_KEY_DOWN, self.on_key_press_event )

    def draw( self, dc ):
        dc.SetBackground( wx.WHITE_BRUSH )
        dc.Clear()

        #dc = wx.GCDC( dc )
        #cr = dc.GetGraphicsContext()
        #ident = cr.CreateMatrix()
        #cr.Scale(self.zoom_ratio, self.zoom_ratio)
        #cr.Translate(self.x-self.graph.width, self.y-self.graph.height/2)

        self.graph.draw(dc, highlight_items=self.highlight)

        #cr.SetTransform( ident )

    def set_filter(self, program):
        self.filter = program

    def set_dotcode(self, dotcode, filename='<stdin>'):
        if isinstance(dotcode, unicode):
            dotcode = dotcode.encode('utf8')
        p = subprocess.Popen(
            [self.filter, '-Txdot'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=False,
            universal_newlines=True
        )
        xdotcode, error = p.communicate(dotcode)
        if p.returncode != 0:
            import dialog
            dialog.ErrorWarning( error, 'Dot Viewer' )
        try:
            self.set_xdotcode(xdotcode)
        except ParseError, ex:
            import dialog
            dialog.ErrorWarning( str(ex), 'Dot Viewer' )
            return False
        else:
            self.openfilename = filename
            return True

    def set_xdotcode(self, xdotcode):
        parser = XDotParser(xdotcode)
        self.graph = parser.parse()
        self.drawOverview()
        self.queue_draw()

    def drawOverview( self ):
        if self.graph.width > self.graph.height:
            w = 128
            h = int( 128 * ( self.graph.height / self.graph.width ) )
        else:
            h = 128
            w = int( 128 * ( self.graph.width / self.graph.height ) )
        self.overview = wx.EmptyBitmap( w, h )
        mdc = wx.MemoryDC( self.overview )
        dc = wx.GCDC( mdc )

        dc.SetBackground( wx.WHITE_BRUSH )
        dc.Clear()
        zoom = min( 128/self.graph.width, 128/self.graph.height )
        dc.SetUserScale( zoom, zoom )
        self.graph.draw( dc )

        mdc.SelectObject( wx.NullBitmap )


    def get_current_pos(self):
        return self.x, self.y

    def set_current_pos(self, x, y):
        self.x = x
        self.y = y
        self.queue_draw()

    def set_highlight(self, items):
        if self.highlight != items:
            self.highlight = items
            # self.queue_draw( force=True )

    def center_image( self, pt=None ):
        if not pt:
            x = self.graph.width/2
            y = self.graph.height/2
        else:
            x, y = pt

        size = self.GetSize()
        x = x*self.zoom_ratio
        y = y*self.zoom_ratio
        xadj = size.width/2
        yadj = -size.height/2
        self.x = x  - xadj
        self.y = y  + yadj
        self.queue_draw()

    def zoom_image(self, zoom_ratio, center=False, pos=None,keeppos=False):
        zoom_ratio = min( 1.5, zoom_ratio )
        zoom_ratio = max( 0.1, zoom_ratio )
        if center:
            self.x = self.graph.width
            self.y = self.graph.height/2
        elif pos:
            x, y = pos
            self.x += x / self.zoom_ratio + x / zoom_ratio
            self.y += y / self.zoom_ratio + y / zoom_ratio
        elif keeppos:
            xadj = .5*self.GetSize().width * ( self.zoom_ratio - zoom_ratio )
            yadj = .5*self.GetSize().height * ( self.zoom_ratio - zoom_ratio )

            scale = self.zoom_ratio / zoom_ratio
            self.x = self.x / scale - xadj
            self.y = self.y / scale - yadj

            self.zoom_ratio = zoom_ratio
        self.zoom_ratio = zoom_ratio
        self.zoom_to_fit_on_resize = False
        self.queue_draw()

    def zoom_to_area(self, x1, y1, x2, y2):
        rect = self.GetRect()
        width = abs(x1 - x2)
        height = abs(y1 - y2)
        self.zoom_ratio = min(
            float(rect.width)/float(width),
            float(rect.height)/float(height)
        )
        self.zoom_to_fit_on_resize = False
        self.x = (x1 + x2) / 2
        self.y = (y1 + y2) / 2
        self.queue_draw()

    def zoom_to_fit(self):
        w,h = self.GetSize().Get()
        rect = wx.Rect(0, 0, w, h)
        rect.x += self.ZOOM_TO_FIT_MARGIN
        rect.y += self.ZOOM_TO_FIT_MARGIN
        rect.width -= 2 * self.ZOOM_TO_FIT_MARGIN
        rect.height -= 2 * self.ZOOM_TO_FIT_MARGIN
        zoom_ratio = min(
            float(rect.width)/float(self.graph.width),
            float(rect.height)/float(self.graph.height)
        )
        self.zoom_image(zoom_ratio, center=False)
        self.zoom_to_fit_on_resize = True
        self.x = 0
        self.y = 0

    ZOOM_INCREMENT = .1
    ZOOM_TO_FIT_MARGIN = 12

    def on_zoom_in(self, action):
        self.zoom_image(self.zoom_ratio + self.ZOOM_INCREMENT)

    def on_zoom_out(self, action):
        self.zoom_image(self.zoom_ratio - self.ZOOM_INCREMENT)

    def on_zoom_fit(self, action):
        self.zoom_to_fit()

    def on_zoom_100(self, action):
        self.zoom_image(1.0)

    POS_INCREMENT = 50
    CTRL_POS_INCREMENT = 150

    def on_key_press_event( self, evt=None, kc=None ):
        kc = kc or evt.GetKeyCode()

        if evt and evt.ControlDown():
            inc = self.CTRL_POS_INCREMENT
        else:
            inc = self.POS_INCREMENT

        if kc == wx.WXK_LEFT:
            self.x -= inc/self.zoom_ratio
            self.queue_draw()
            return True
        elif kc == wx.WXK_RIGHT:
            self.x += inc/self.zoom_ratio
            self.queue_draw()
            return True
        elif kc == wx.WXK_UP:
            self.y -= inc/self.zoom_ratio
            self.queue_draw()
            return True
        elif kc == wx.WXK_DOWN:
            self.y += inc/self.zoom_ratio
            self.queue_draw()
            return True
        elif kc in [wx.WXK_PAGEUP, ord('+'), ord('=')]:
            zoom = self.zoom_ratio + self.ZOOM_INCREMENT
            self.zoom_image( zoom, keeppos=True )
            self.queue_draw()
            return True
        elif kc in [wx.WXK_PAGEDOWN, ord('-')]:
            self.zoom_image( self.zoom_ratio - self.ZOOM_INCREMENT, keeppos=True )
            self.queue_draw()
            return True
        elif kc == wx.WXK_ESCAPE:
            self.drag_action.abort()
            self.drag_action = NullAction(self)
            return True
        elif kc == ord( '0' ):
            self.zoom_image(1.0)
        elif kc == ord( 'F' ):
            self.zoom_to_fit()
        elif kc == ord( 'C' ):
            self.center_image()
        #if kc == gtk.keysyms.r:
            #if self.openfilename is not None:
                #try:
                    #fp = file(self.openfilename, 'rt')
                    #self.set_dotcode(fp.read(), self.openfilename)
                    #fp.close()
                #except IOError, ex:
                    #pass
            #return True

        return False

    def get_drag_action(self, event):
        if event.ControlDown():
            return ZoomAction
        elif event.ShiftDown():
            return ZoomAreaAction
        else:
            return PanAction
        return NullAction

    def overviewClick( self, x, y, dragging=False, release=False ):
        size = self.GetSize()
        osize = self.overview.GetSize()
        ox = osize.width - ( size.width - x )
        oy = osize.height - ( size.height - y )
        if ox > 0 and oy > 0:
            zw = osize.width * size.width / self.graph.width / self.zoom_ratio
            zh = osize.height * size.height / self.graph.height / self.zoom_ratio
            ox -= zw / 2
            oy -= zh / 2
            x2 = ( ox/float( osize.width ) ) * self.graph.width
            y2 = ( oy/float( osize.height ) ) * self.graph.height
            x2 = x2 * self.zoom_ratio
            y2 = y2 * self.zoom_ratio
            if dragging:
                self.x = x2
                self.y = y2
                self.queue_draw()
            elif release:
                self.animate_to( x2, y2, zoom=False )
            return True
        return False

    def on_area_button_press(self, event):
        self.CaptureMouse()
        self.animation.stop()
        self.drag_action.abort()
        self.dragging = False
        x, y = event.GetX(), event.GetY()
        if self.overviewClick( x, y, dragging=False, release=False ):
            return
        action_type = self.get_drag_action(event)
        self.drag_action = action_type(self)
        self.drag_action.on_button_press(event)

    def on_area_button_release(self, event):
        if self.HasCapture():
            self.ReleaseMouse()
        dragging = self.dragging
        self.dragging = False
        x, y = event.GetX(), event.GetY()
        if self.overviewClick( x, y, dragging=dragging, release=True ):
            return

        self.drag_action.on_button_release(event)
        self.drag_action = NullAction(self)
        if not dragging:
            x += self.x
            y += self.y
            code_jump = self.get_code_jump( x, y )
            

    def on_area_motion_notify(self, event):
        self.dragging = event.Dragging()
        if event.LeftIsDown() and self.overviewClick( event.GetX(), event.GetY(), self.dragging ):
            return
        self.drag_action.on_motion_notify(event)

    def animate_to(self, x, y, zoom=True):
        if zoom:
            self.animation = ZoomToAnimation(self, x, y)
        else:
            self.animation = MoveToAnimation(self, x, y)


        self.animation.start()

    def window2graph(self, x, y):
        #rect = self.GetRect()
        #x -= 0.5*rect.width
        #y -= 0.5*rect.height
        x /= self.zoom_ratio
        y /= self.zoom_ratio
        #x += self.x
        #y += self.y
        return x, y

    def get_url(self, x, y):
        x, y = self.window2graph(x, y)
        return self.graph.get_url(x, y)

    def get_jump(self, x, y):
        x, y = self.window2graph(x, y)
        return self.graph.get_jump(x, y)

    def get_code_jump(self,x,y):
        x, y = self.window2graph(x, y)
        return self.graph.get_code_jump(x, y)

def run( filename='', title='' ):
    dotcode = open(filename, 'rb').read()
    size=(800,700)
    pos=(200,0)
    wx = sys.modules.get('wx')
    
    f = wx.Frame(None, size=size, pos=pos, title=title)
    w = DotWidget(f)
    w.set_dotcode(dotcode, filename)
    f.Show()
    return f
