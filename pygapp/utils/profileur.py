
from cProfile import Profile
from pstats import Stats
import cStringIO
import re
import sys
import traceback
import wx
import types
import wx.lib.mixins.listctrl as listmix

STDOUT=1
STDERR=2

class StringOut(object):
    """Redirect print output to a string. Usage is:

    with StringOut() as s:
        print 'foo'
        myfunction()

    All the prints will be captured to s.buf. You can convert it to a string by doing s.getString()"""

    def __init__(self,flags=STDOUT):
        self.buf = cStringIO.StringIO()
        self.flags = flags

    def __context__(self): return self
    def __enter__(self):
        if self.flags & STDOUT:
            self.oldStdout  = sys.stdout
            sys.stdout      = self.buf
        if self.flags & STDERR:
            self.oldStderr  = sys.stderr
            sys.stderr      = self.buf
        return self

    def __exit__(self,*exc):
        if self.flags & STDOUT:
            sys.stdout      = self.oldStdout
        if self.flags & STDERR:
            sys.stderr      = self.oldStderr

    def getString(self):
        """Return the contents of the buffer as a string"""
        return self.buf.getvalue()


################################################################################
#
# Simple profiler api for user apps.
#
################################################################################


_profiler = None

_IsEnabled = False

def enable():
    global _profiler
    _profiler = Profile()
    _profiler.enable()
    global _IsEnabled
    _IsEnabled = True

def isEnabled():
    global _IsEnabled
    return _IsEnabled

def print_stats( num=100, sortOrder=('cumulative','name') ):
    _profiler.disable()
    stats = Stats( _profiler )
    stats.strip_dirs()
    stats.sort_stats(*sortOrder)
    stats.print_stats( num )
    stats.print_callers( num )
    _profiler.enable()

def disable( printStats=False, num=100 ):
    if printStats:
        print_stats( num=num )
    _profiler.disable()
    global _IsEnabled
    _IsEnabled = False
    return Stats( _profiler )

def clear():
    _profiler.clear()
    _IsEnabled = False

class ProfileCtx( object ):
    def __init__(   self,
                    enable       = True,
                    printOnExit  = True,
                    printCallers = True,
                    dumpOnExit   = False,
                    dumpFile     = None, ):
        if enable:
            self.prof = Profile()
        else:
            self.prof = None
        self.printOnExit  = printOnExit
        self.printCallers = printCallers
        self.dumpOnExit   = dumpOnExit
        self.dumpFile     = dumpFile

    def __enter__( self ):
        if self.prof:
            self.prof.enable()
        return self

    def __exit__( self, *exc_info ):
        if self.prof:
            self.prof.disable()
            if self.printOnExit:
                self.print_stats()
            if self.dumpOnExit:
                self.dump_stats()

    def snapshot_stats( self, num=100, sortOrder=('cumulative','name') ):
        if self.prof:
            self.prof.disable()
            self.print_stats( num, sortOrder )
            self.prof.clear()
            self.prof.enable()

    def print_stats( self, num=100, sortOrder=('cumulative','name') ):
        stats = Stats( self.prof )
        stats.strip_dirs()
        stats.sort_stats( *sortOrder )
        stats.print_stats( num )
        if self.printCallers:
            stats.print_callers( num )

    def dump_stats( self, fileOverride = None ):
        stats = Stats( self.prof )
        if fileOverride is not None or self.dumpFile is not None:
            stats.dump_stats( fileOverride or self.dumpFile )

################################################################################
#
# A GUI version
#
################################################################################

class ProfileList(wx.ListCtrl,listmix.ListCtrlAutoWidthMixin):
    
    def __init__(self,profileFrame,parent,ID,**kwargs):
        wx.ListCtrl.__init__(self,parent,ID,**kwargs)
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        self.profileFrame = profileFrame
        self.attr1 = wx.ListItemAttr()
        #self.attr1.SetBackgroundColour( pie.utils.ROW1_COLOR )

        self.SetFont( wx.Font( 9, wx.TELETYPE, wx.NORMAL, wx.NORMAL ))

    def OnGetItemText(self, item, col):
        stats = self.profileFrame.stats_list
        return stats[item]

def wrapMenu( cb ):
    def f(event):
        if type(cb) == tuple:
            cb[0]( event, *cb[1:] )
        else:
            cb( event )
    return f

def create_menus(frame, menus, bar=None, wrapper=None):
    if not bar:
        bar = wx.MenuBar()

    for name,items in menus:
        menu = create_submenu( frame, items,wrapper )
        bar.Append( menu, name )
    frame.SetMenuBar( bar )

def create_submenu( frame, items, wrapper=None ):
    '''
    items = [ callback, name, [ kwargs, [checked]]]

    if you want to say create a menu item as checked, then you can do:
    [
       ( f, 'checked',    { 'kind': wx.ITEM_CHECK }, True ),  # check will be checked by default
       ( f, 'notchecked', { 'kind': wx.ITEM_CHECK }, False ), # notchecked will be notchecked by default
    ]
    '''
    m = wx.Menu()
    for item in items:
        if item == None:
            m.AppendSeparator()
        elif type(item) == types.DictType:
            name = item[ 'name' ]
            submenu = create_submenu( frame, item[ 'items' ], wrapper=wrapper )
            m.AppendMenu( wx.NewId(), name, submenu )
        else:
            callback = wrapper and wrapper(item[0]) or item[0]
            name = item[1]
            id = wx.NewId()
            kwargs = {}
            if len(item) > 2:
                kwargs = item[2]
            mi = m.Append( id, name, **kwargs )
            if len(item) > 3:
                if item[3]:
                    mi.Check()
            if callback:
                frame.Bind( wx.EVT_MENU, callback, id=id )
    return m

class ProfilerFrame( wx.Frame ):
    
    def __init__(self, title='Profileur', stats=None ):
        
        wx.Frame.__init__(self, None, -1, title=title, pos=wx.DefaultPosition, size=(640,480))
        self.splitter = wx.SplitterWindow( self )
        self.panel = wx.Panel( self.splitter, style=wx.TAB_TRAVERSAL )
        self.callers = wx.TextCtrl( self.splitter, -1, '', style=wx.TE_MULTILINE|wx.TE_DONTWRAP|wx.TE_READONLY )
        self.callers.SetFont( wx.Font( 9, wx.TELETYPE, wx.NORMAL, wx.NORMAL ))
        self.splitter.SplitHorizontally( self.panel, self.callers, -100 )

        icon = wx.EmptyIcon()
        #icon.CopyFromBitmap(lib.pieimages.getProfileBitmap())
        self.SetIcon(icon)

        randomId = wx.NewId()
        self.Bind(wx.EVT_MENU, self.onEscape, id=randomId)
        
        self.SetAcceleratorTable(wx.AcceleratorTable([
             (wx.ACCEL_NORMAL, wx.WXK_ESCAPE, wx.ID_CANCEL), ] ) )


        sizer  = wx.BoxSizer(  wx.VERTICAL )
        box    = wx.StaticBox( self.panel, -1, 'Profiler state' )

        self.search      = wx.SearchCtrl( self.panel, size=(150,-1), style=wx.TE_PROCESS_ENTER )
        self.viewGraph   = wx.Button( self.panel, -1, label='View Graph...' )
        self.viewGraph.Enable(False)

        # size em
        bsizer           = wx.StaticBoxSizer( box, wx.HORIZONTAL )
        bsizer.AddMany([ self.search ])
        bsizer.AddStretchSpacer(1)
        bsizer.Add( self.viewGraph )

        sizer.Add( bsizer, 0, wx.EXPAND )

        self.list = ProfileList(self, self.panel,-1,style=wx.LC_REPORT|wx.LC_VIRTUAL|wx.BORDER_NONE)
        self.list.InsertColumn( 0, '' )
        sizer.Add( self.list, 3, wx.EXPAND )
        self.panel.SetSizer( sizer )

        create_menus(self, (
            ('&File',(
                (self.OnSave, '&Save stats'),
                (self.OnLoad, '&Load Stats'),
            ), ),
            ('&View',(
                ((self.OnShowStats, 'print_callers'), 'Show &Callers'),
                ((self.OnShowStats, 'print_callees'), 'Show Call&ees'),
                ((self.OnShowStats, 'print_stats'), 'Show &Stats'),
                ((self.OnSort, ('calls','name')), 'Sort Calls'),
                ((self.OnSort, ('time','name')), 'Sort Time'),
                ((self.OnSort, ('cumulative','name')), 'Sort CumTime'),
            ),)
        ), wrapper=wrapMenu )

        self.profiler = _profiler
        self.sort = ('time','name')
        self.print_func = 'print_stats'
        self.stats = Stats( _profiler )
        self.stats_list = []
        self.func_lines = []
        
        self.list.SetItemCount( 0 )
        self.showStats()

        self.Bind( wx.EVT_BUTTON, self.OnViewGraph, self.viewGraph )
        self.Bind( wx.EVT_CLOSE, self.OnClose )
                
        self.Bind( wx.EVT_TEXT_ENTER, self.OnTextEntered, self.search )
        self.Bind( wx.EVT_LIST_ITEM_SELECTED, self.OnListItemSelected, self.list )
        self.Bind( wx.EVT_LIST_KEY_DOWN, self.OnListKeyDown, self.list )

        self.list.SetFocus()

    def setStream( self ):
        self.stats.stream = sys.stdout

    def OnListItemSelected( self, event ):
        
        idx = self.list.GetFirstSelected()
        
        func = idx != -1 and idx < len( self.func_lines ) and self.func_lines[ idx ]
        if func:
            with StringOut() as buf:
                self.setStream()
                _, _, _, _, callers = self.stats.stats[ self.func_lines[ idx ]]
                self.stats.print_call_line(40, func, callers)
            self.setStream()
            self.callers.SetValue( buf.getString() )
        else:
            self.callers.SetValue( '' )
            
        event.Skip()

    def OnTextEntered( self, event ):
        
        text = self.search.GetValue()
        rex = re.compile( text )
        self.stats_list = [ x for x in self.stats_list_orig if rex.search( x[0] ) ]
        self.list.SetItemCount( len(self.stats_list) )
        self.list.Refresh()

    def OnShowStats( self, event, n ):
        self.print_func = n
        if self.stats:
            self.collectOutput()


    def OnListKeyDown(self, event):
        pass#print 'OnListKeyDown', event.GetKeyCode()
                
    def OnSort( self, event, key ):
        self.sort = key
        if self.stats:
            self.collectOutput()

    def onEscape(self, event):
        """"""
        self.OnClose(event)
             
    def collectOutput(self):
        try:
            self.func_lines = []
            with StringOut() as buf:
                self.setStream()
                self.stats.sort_stats( *self.sort )
                getattr( self.stats, self.print_func )( 500 )
            self.setStream()
            self.stats_list = self.stats_list_orig = buf.getString().splitlines()

            # bit of a hack, we know there are 2 lines at the end, so just get the func_lines in the right order
            self.func_lines.extend([ None, None ])
            self.func_lines[0:0] = [ None for _ in xrange( len( self.stats_list ) - len( self.func_lines )) ]

            self.list.SetItemCount( len(self.stats_list) )
            self.list.Refresh()

        except:
            traceback.print_exc()

    def OnClose(self,event):
        self.profiler.disable()
        self.Destroy()

    def my_print_line( self, func ):
        self.func_lines.append( func )
        return Stats.print_line( self.stats, func )

    def showStats( self ):
        self.stats.print_line = self.my_print_line
        self.collectOutput()
        self.viewGraph.Enable(True)

    def OnViewGraph(self, event):
        import gprof2dot, dialog, xdot
        
        vals = dialog.TextExt('Profile Graph', ['Node Threshold', 'Edge Threshold'], ['2', '1'] )
        if not vals:
            return

        node_thres, edge_thres = map( float, vals )
        path = gprof2dot.pstats2dot(self.stats, node_thres=node_thres, edge_thres=edge_thres)
        xdot.run( path )
        

    def OnSave( self, event ):
        if self.stats:
            import dialog
            f = dialog.FileSave( text='File to save stats:', title='Save Stats', filter='*.profile' )
            if f and f.paths:
                self.stats.dump_stats( f.paths[0] )

    def OnLoad( self, event ):
        
        import dialog
        f = dialog.File( text='Stats file:', title='Load Stats', filter='*.profile' )
        if f and f.paths:
            self.stats = Stats( f.paths[0] )
            self.showStats()


def Show():
    app = wx.App()
    f = ProfilerFrame()
    f.Show()
    app.MainLoop()
    return f
