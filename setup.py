#!/usr/bin/env python

from distutils.core import setup


setup(name='pygapp',
      version='1.0',
      description='Pygame Application Base',
      author='Guillaume Giraud',
      author_email='guillaume.giraud@gmail.com',
      url='',
      packages=['pygame'],
     )